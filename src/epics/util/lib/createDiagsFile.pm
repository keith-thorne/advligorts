package CDS::createDiagsFile;
use Exporter;
@ISA = ('Exporter');

#// \b sub \b writeDiagsFile \n
#//  Write file with list of all parts and their connections. \n\n
sub writeDiagFile {
my ($fileName) = @_;
#open(OUTD,">$fileName") || die "cannot open compile warnings output file for writing";
$diag = "./diags\.txt";
open(OUTD,">".$diag) || die "cannot open diag file for writing";
for ($ll = 0; $ll < $::subSys; $ll++) {
  $xx = $::subSysPartStop[$ll] - $::subSysPartStart[$ll]; # Parts count for this subsystem
  $subCntr[$ll] = 0;
  #print "SubSys $ll $subSysName[$ll] from $subSysPartStart[$ll] to $subSysPartStop[$ll]\n";
  print OUTD "\nSubSystem $ll has $xx parts ************************************\n";
  for ($ii = $::subSysPartStart[$ll]; $ii < $::subSysPartStop[$ll]; $ii++) {
    print OUTD "Part $ii $::xpartName[$ii] is type $::partType[$ii] with $::partInCnt[$ii] inputs and $::partOutCnt[$ii] outputs\n";
    print OUTD "INS FROM:\n";
    print OUTD "\tPart Name\tType\tNum\tPort\n";
    for($jj=0;$jj<$::partInCnt[$ii];$jj++) {
      #$from = $partInNum[$ii][$jj];
      print OUTD "\t$::partInput[$ii][$jj]\t$::partInputType[$ii][$jj]\t$::partInNum[$ii][$jj]\t$::partInputPort[$ii][$jj]\n";
      if (($::partType[$ii] eq "INPUT") && ($::partOutCnt[$ii] > 0)) {
	print OUTD "From Subsystem $::partSysFrom[$ii]\n";
	$subInputs[$ll][$subCntr[$ll]] = $::partSysFrom[$ii];
	$subInputsType[$ll][$subCntr[$ll]] = $::partInputType[$ii][$jj];
	$subCntr[$ll] ++;
      }
    }
    print OUTD "OUT TO:\n";
    print OUTD "\tPart Name\tType\tNum\tPort\tPort Used\n";
    for($jj = 0;$jj < $::partOutCnt[$ii]; $jj++) {
      #$to = $partOutNum[$ii][$jj];
      print OUTD "\t$::partOutput[$ii][$jj]\t$::partOutputType[$ii][$jj]\t$::partOutNum[$ii][$jj]\t$::partOutputPort[$ii][$jj]\t$::partOutputPortUsed[$ii][$jj]\n";
    }
    print OUTD "\n****************************************************************\n";
  }
}
print OUTD "Non sub parts ************************************\n";
for ($ii = 0; $ii < $::nonSubCnt; $ii++) {
  $xx = $::nonSubPart[$ii];
  print OUTD "Part $xx $::xpartName[$xx] is type $::partType[$xx] with $::partInCnt[$xx] inputs and $::partOutCnt[$xx] outputs\n";
  print OUTD "INS FROM:\n";
  for ($jj = 0; $jj < $::partInCnt[$xx]; $jj++) {
    #$from = $partInNum[$xx][0];
    if ($::partSysFromx[$xx][$jj] == -1) {
      print OUTD "\t$::partInput[$xx][$jj]\t$::partInputType[$xx][$jj]\t$::partInNum[$xx][$jj]\t$::partInputPort[$xx][$jj] subsys NONE\n";
    } else {
      print OUTD "\t$::partInput[$xx][$jj]\t$::partInputType[$xx][$jj]\t$::partInNum[$xx][$jj]\t$::partInputPort[$xx][$jj] subsys $::partSysFromx[$xx][$jj]\n";
    }
  }
  print OUTD "OUT TO:\n";
  print OUTD "\tPart Name\tType\tNum\tPort\tPort Used\n";
  for ($jj = 0; $jj < $::partOutCnt[$xx]; $jj++) {
    $to = $::partOutNum[$xx][$jj];
    print OUTD "\t$::partOutput[$xx][$jj]\t$::partOutputType[$xx][$jj]\t$::partOutNum[$xx][$jj]\t$::partOutputPort[$xx][$jj]\t$::partOutputPortUsed[$xx][$jj]\n";
  }
  print OUTD "\n****************************************************************\n";
}
for ($ii = 0; $ii < $::subSys; $ii++) {
  print OUTD "\nSUBS $ii $::subSysName[$ii] *******************************\n";
  for($ll=0;$ll<$::subCntr[$ii];$ll++) {
    print OUTD "$ll $::subInputs[$ii][$ll] $::subInputsType[$ii][$ll]\n";
  }
}
close(OUTD);
#Call a python script to create ADC channel list file
$rcg_parser =  $::rcg_src_dir;
$rcg_parser .= "/src/epics/util/adcparser.py ";
#$rcg_parser .= $fileName;
my $cpcmd = "cp ";
$cpcmd .= " ./diags.txt ";
$cpcmd .= $fileName;
system($rcg_parser);
sleep(2);
system($cpcmd);
# End DIAGNOSTIC
}
