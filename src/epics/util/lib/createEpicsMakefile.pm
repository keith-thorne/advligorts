package CDS::EpicsMakefile;
use Exporter;
@ISA = ('Exporter');


#// \b sub \b createEpicsMakefile \n
#// Create the EPICS Makefile \n\n
sub createEpicsMfile {
my ($fileName) = @_;

	open(OUTME,">./".$fileName) || die "cannot open EPICS Makefile file for writing";
	print OUTME "\n";
	print OUTME "# Define Epics system name. It should be unique.\n";
	print OUTME "TARGET = $::skeleton";
	print OUTME "epics\n";
	print OUTME "\n";
	print OUTME "SRC = build/\$(TARGET)/";
	print OUTME "$::skeleton";
	print OUTME "\.st\n";
	print OUTME "\n";
	if ($edcu) {
	print OUTME "SRC += $::rcg_src_dir/src/epics/seq/edcu.c\n";
	}elsif ($globalsdf) {
	print OUTME "SRC += $::rcg_src_dir/src/epics/seq/sdf_monitor.c\n";
	} else {
	print OUTME "SRC += $::rcg_src_dir/src/epics/seq/main.c\n";
	}
	print OUTME "SRC += $::rcg_src_dir/src/drv/rfm.c\n";
	print OUTME "SRC += $::rcg_src_dir/src/drv/param.c\n";
	print OUTME "SRC += $::rcg_src_dir/src/drv/crc.c\n";
	print OUTME "SRC += $::rcg_src_dir/src/drv/fmReadCoeff.c\n";
	#print OUTME "SRC += src/epics/seq/get_local_time.st\n";
	if ($::userspacegps)
	{
		print OUTME "SRC += $::rcg_src_dir/src/drv/gpsclock.c\n";
	}
	for($ii=0;$ii<$::useWd;$ii++)
	{
		print OUTME "SRC += src/epics/seq/hepiWatchdog";
		print OUTME "\U$::useWdName[$ii]";
		print OUTME "\L\.st\n";
	}
	print OUTME "\n";
	#print OUTME "DB += src/epics/db/local_time.db\n";
	print OUTME "DB += build/\$(TARGET)/";
	print OUTME "$::skeleton";
	print OUTME "1\.db\n";
	print OUTME "\n";
	print OUTME "IFO = $::ifo\n";
	print OUTME "SITE = $::lsite\n";
	print OUTME "\n";
	# The CA SDF build does not need a SEQUENCER added
	if ($::casdf==0) {
	print OUTME "SEQ += \'";
	print OUTME "$::skeleton";
	print OUTME ",(\"ifo=$::ifo, site=$::lsite, sys=\U$::systemName\, \Lsysnum=$::dcuId\, \Lsysfile=\U$::skeleton \")\'\n";
	}
	#print OUTME "SEQ += \'get_local_time,(\"ifo=$ifo, sys=\U$systemName\")\'\n";
	for($ii=0;$ii<$::useWd;$ii++)
	{
	print OUTME "SEQ += \'";
	print OUTME "hepiWatchdog";
	print OUTME "\U$::useWdName[$ii]";
	print OUTME ",(\"ifo=$::ifo, sys=\U$::systemName\,\Lsubsys=\U$::useWdName[$ii]\")\'\n";
	}
	print OUTME "\n";
	print OUTME "EXTRA_CFLAGS += -D";
	print OUTME "\U$::skeleton";
	print OUTME "_CODE\n";
	print OUTME "EXTRA_CFLAGS += -DFE_HEADER=\\\"\L$::skeleton.h\\\"\n";
	if ($edcu) {
	  print OUTME "EXTRA_CFLAGS += -DEDCU=1\n";
	  print OUTME "EXTRA_CFLAGS += -DNO_DAQ_IN_SKELETON=1\n";
	}
	if ($::globalsdf) {
	  print OUTME "EXTRA_CFLAGS += -DEDCU=1\n";
	  print OUTME "EXTRA_CFLAGS += -DNO_DAQ_IN_SKELETON=1\n";
	}
	if ($::casdf) {
	  print OUTME "EXTRA_CFLAGS += -DCA_SDF=1\n";
	  print OUTME "EXTRA_CFLAGS += -DUSE_SYSTEM_TIME=1\n";
	}
	print OUTME "\n";
	#print OUTME "LIBFLAGS += -lezca\n";
	if($::systemName eq "sei" || $::useFIRs)
	{
	print OUTME "EXTRA_CFLAGS += -DFIR_FILTERS\n";
	}
	if ($::userspacegps)
	{
		print OUTME "EXTRA_CFLAGS += -DUSE_GPSCLOCK\n";
	}
	print OUTME "include $::rcg_src_dir/config/Makefile.linux\n";
	print OUTME "\n";
	print OUTME "build/\$(TARGET)/";
	print OUTME "$::skeleton";
	print OUTME "1\.db: build/\$(TARGET)/";
	print OUTME "$::skeleton";
	print OUTME "\.db\n";
	print OUTME "\tsed 's/%SYS%/";
	print OUTME "\U$::systemName";
	print OUTME "/g;s/%SUBSYS%//g' \$< > \$\@\n";
	print OUTME "\n";
	print OUTME "\n";
	close OUTME;
}
