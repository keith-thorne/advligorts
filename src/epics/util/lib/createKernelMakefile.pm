package CDS::KernelMakefile;
use Exporter;
@ISA = ('Exporter');

#// \b sub \b createCmakefile \n
#// Generate the user C code Makefile  \n\n
sub createCmakefile{
my ($fileName) = @_;

# Compile options common to all runtime configurations
system ("/bin/cp GNUmakefile  ../../fe/$::skeleton");
open(OUTM,">./".$fileName) || die "cannot open Makefile file for writing";

print OUTM "# CPU-Shutdown Real Time Linux\n";
print OUTM "KBUILD_EXTRA_SYMBOLS=$::rcg_src_dir/src/drv/ExtraSymbols.symvers\n";
print OUTM "KBUILD_EXTRA_SYMBOLS += $::mbufsymfile\n";
print OUTM "KBUILD_EXTRA_SYMBOLS += $::gpssymfile\n";
print OUTM "KBUILD_EXTRA_SYMBOLS += \$(PWD)/ModuleIOP.symvers\n";
print OUTM "ALL \+= user_mmap \$(TARGET_RTL)\n";
print OUTM "EXTRA_CFLAGS += -O -w -I../../include\n";

print OUTM "EXTRA_CFLAGS += $::servoflag \n";

print OUTM "EXTRA_CFLAGS += -D";
print OUTM "\U$::skeleton";
print OUTM "_CODE\n";
print OUTM "EXTRA_CFLAGS += -DFE_SRC=\\\"\L$::skeleton/\L$::skeleton.c\\\"\n";
print OUTM "EXTRA_CFLAGS += -DFE_HEADER=\\\"\L$::skeleton.h\\\"\n";

# Model uses FIR filters
if($::systemName eq "sei" || $::useFIRs)
{
print OUTM "EXTRA_CFLAGS += -DFIR_FILTERS\n";
}
print OUTM "EXTRA_CFLAGS += -g\n";

if ($::remoteGPS) {
  print OUTM "EXTRA_CFLAGS += -DREMOTE_GPS\n";
}
if ($::requireIOcnt) {
  print OUTM "#Comment out to disenable exact IO module count requirement\n";
  print OUTM "EXTRA_CFLAGS += -DREQUIRE_IO_CNT\n";
} else {
  print OUTM "#Uncomment to enable exact IO module count requirement\n";
  print OUTM "#EXTRA_CFLAGS += -DREQUIRE_IO_CNT\n";
}
if (0 == $::dac_testpoint_names && 0 == $::extraTestPoints && 0 == $::filtCnt) {
	print "Not compiling DAQ into the front-end\n";
	$::no_daq = 1;
}
if ($::edcu) {
    $::no_daq = 1;
}
if ($::no_daq) {
  print OUTM "#Comment out to enable DAQ\n";
  print OUTM "EXTRA_CFLAGS += -DNO_DAQ\n";
}

# Set to flip polarity of ADC input signals
if ($::flipSignals) {
  print OUTM "EXTRA_CFLAGS += -DFLIP_SIGNALS=1\n";
}
# Set CPU core for runtime
if ($::specificCpu > -1) {
  print OUTM "#Comment out to run on first available CPU\n";
  print OUTM "EXTRA_CFLAGS += -DSPECIFIC_CPU=$::specificCpu\n";
}

# Set BIQUAD as default starting RCG V2.8
  print OUTM "EXTRA_CFLAGS += -DALL_BIQUAD=1 -DCORE_BIQUAD=1\n";

if ($::rfmDelay) {
  print OUTM "#Comment out to run without RFM Delayed by 1 cycle\n";
  print OUTM "EXTRA_CFLAGS += -DRFM_DELAY=1\n";
}

#********* END OF COMMON COMPILE OPTIONS

if ($::iopModel > -1) {  #************ SETUP FOR IOP ***************
  print OUTM "EXTRA_CFLAGS += -DIOP_MODEL\n";
  $modelType = "IOP";
  if($::diagTest > -1) {
  print OUTM "EXTRA_CFLAGS += -DDIAG_TEST\n";
  }
  if($::internalclk) {
  print OUTM "EXTRA_CFLAGS += -DUSE_ADC_CLOCK\n";
  }
# Invoked if IOP cycle rate slower than ADC clock rate
  print OUTM "EXTRA_CFLAGS += -DUNDERSAMPLE=$::clock_div\n";
  $adccopyrate = $::modelrate / $::adcclock;
  print OUTM "EXTRA_CFLAGS += -DADC_MEMCPY_RATE=$adccopyrate\n";

#Following used for IOP running at 128K 
  if($::adcclock ==128) {
  print OUTM "EXTRA_CFLAGS += -DIOP_IO_RATE=131072\n";
  } else {
  print OUTM "EXTRA_CFLAGS += -DIOP_IO_RATE=65536\n";
  }

#Following used for testing 
  if($::dacWdOverride > -1) {
  print OUTM "EXTRA_CFLAGS += -DDAC_WD_OVERRIDE\n";
  }
#Following set to run as standard kernel module
  if ($::no_cpu_shutdown > 0) {
    print OUTM "EXTRA_CFLAGS += -DNO_CPU_SHUTDOWN\n";
  } else {
    print OUTM "#EXTRA_CFLAGS += -DNO_CPU_SHUTDOWN\n";
  }
  # ADD DAC_AUTOCAL to IOPs
  print OUTM "EXTRA_CFLAGS += -DDAC_AUTOCAL\n";

# Set to run without LIGO timing system
  if ($::no_sync == 1) {
    print OUTM "EXTRA_CFLAGS += -DNO_SYNC\n";
  }  
# Test mode to force sync to 1pps even if TDS present
  if ($::test1pps == 1) {
    print OUTM "EXTRA_CFLAGS += -DTEST_1PPS\n";
  }  
# Set to run IOP as time master for the Dolphin Network
  if ($::dolphin_time_xmit > -1) {
    print OUTM "EXTRA_CFLAGS += -DXMIT_DOLPHIN_TIME=1\n";
  }
# Set to run IOP as time receiver on the Dolphin Network
  if ($::dolphinTiming > -1 or $::virtualiop == 2) {
    print OUTM "EXTRA_CFLAGS += -DUSE_DOLPHIN_TIMING\n";
    print OUTM "EXTRA_CFLAGS += -DNO_DAC_PRELOAD=1\n";
  }
# Set to run IOP on internal clock ie no IO modules
  if ($::virtualiop == 1) {
    print OUTM "EXTRA_CFLAGS += -DRUN_WO_IO_MODULES=1\n";
    print OUTM "EXTRA_CFLAGS += -DNO_DAC_PRELOAD=1\n";
  }
# Set IOP to map Dolphin Networks
# Dolphin Gen2 is the default
 if ($::virtualiop != 1) {
  if ($::pciNet > 0) {
        if ($::dolphinGen == 2) {
          print OUTM "#Enable use of PCIe RFM Network Gen 2\n";
          print OUTM "DISDIR = /opt/srcdis\n";
          print OUTM "KBUILD_EXTRA_SYMBOLS += \$(DISDIR)/src/SCI_SOCKET/ksocket/LINUX/Module.symvers\n";
          print OUTM "EXTRA_CFLAGS += -DOS_IS_LINUX=1 -D_DIS_KERNEL_=1 -I\$(DISDIR)/src/IRM_GX/drv/src -I\$(DISDIR)/src/IRM_GX/drv/src/LINUX -I\$(DISDIR)/src/include -I\$(DISDIR)/src/include/dis -I\$(DISDIR)/src/COMMON/osif/kernel/include -I\$(DISDIR)/src/COMMON/osif/kernel/include/LINUX -DDOLPHIN_TEST=1  -DDIS_BROADCAST=0x80000000\n";
	} else {
          print OUTM "#Enable use of PCIe RFM Network Gen 1\n";
          print OUTM "DISDIR = /opt/srcdis\n";
          print OUTM "KBUILD_EXTRA_SYMBOLS += \$(DISDIR)/src/SCI_SOCKET/ksocket/lib/LINUX/Module.symvers\n";
          print OUTM "EXTRA_CFLAGS += -DOS_IS_LINUX=1 -D_KERNEL=1 -I\$(DISDIR)/src/IRM/drv/src -I\$(DISDIR)/src/IRM/drv/src/LINUX -I\$(DISDIR)/src/include -I\$(DISDIR)/src/include/dis -DDOLPHIN_TEST=1  -DDIS_BROADCAST=0x80000000\n";
        }
  }
 }

  if ($::optimizeIO) {
    print OUTM "EXTRA_CFLAGS += -DNO_DAC_PRELOAD=1\n";
  }
  if ($::lhomid) {
    print OUTM "EXTRA_CFLAGS += -DADD_SEC_ON_START=1\n";
  }
}
# End of IOP compile options

if ($::iopModel < 1) {   #************ SETUP FOR USER APP ***************
  print OUTM "EXTRA_CFLAGS += -DCONTROL_MODEL\n";
  print OUTM "EXTRA_CFLAGS += -DUNDERSAMPLE=1\n";
  print OUTM "EXTRA_CFLAGS += -DADC_MEMCPY_RATE=1\n";
  $modelType = "CONTROL";

  if ($::noZeroPad) {
    print OUTM "EXTRA_CFLAGS += -DNO_ZERO_PAD=1\n";
  }
  if ($::biotest) {
    print OUTM "EXTRA_CFLAGS += -DDIO_TEST=1\n";
  }

#Following used with IOP running at 128K 
  if($::adcclock ==128) {
    print OUTM "EXTRA_CFLAGS += -DIOP_IO_RATE=131072\n";
    if($::modelrate < 128) {
        my $drate = 128/$::modelrate;
        if($drate == 8 or $drate == 2 or $drate > 32) {
            die "RCG does not support a user model rate $::modelrate" . "K with IOP data at $::adcclock" ."K\n"  ;
        }
        print OUTM "EXTRA_CFLAGS += -DOVERSAMPLE\n";
        print OUTM "EXTRA_CFLAGS += -DOVERSAMPLE_DAC\n";
        print OUTM "EXTRA_CFLAGS += -DOVERSAMPLE_TIMES=$drate\n";
        print OUTM "EXTRA_CFLAGS += -DFE_OVERSAMPLE_COEFF=feCoeff$drate"."x\n";
    }
  }

#Following used with IOP running at 64K (NORMAL) 
  if($::adcclock ==64) {
    print OUTM "EXTRA_CFLAGS += -DIOP_IO_RATE=65536\n";
    if($::modelrate < 64) {
        my $drate = 64/$::modelrate;
        if($drate == 8 or $drate > 32) {
            die "RCG does not support a user model rate $::modelrate" . "K with IOP data at $::adcclock" ."K\n"  ;
        }
        print OUTM "EXTRA_CFLAGS += -DOVERSAMPLE\n";
        print OUTM "EXTRA_CFLAGS += -DOVERSAMPLE_DAC\n";
        print OUTM "EXTRA_CFLAGS += -DOVERSAMPLE_TIMES=$drate\n";
        print OUTM "EXTRA_CFLAGS += -DFE_OVERSAMPLE_COEFF=feCoeff$drate"."x\n";
    }
  }

}  #******************* END SETUP FOR USER APP



print OUTM "\n";
print OUTM "ifneq (\$(CDIR),)\n";
print OUTM "override EXTRA_CFLAGS += \$(patsubst %,-I../../../%,\$(CDIR))\n";
print OUTM "endif\n";

print OUTM "\n";
print OUTM "all: \$(ALL)\n";
print OUTM "\n";
print OUTM "clean:\n";
print OUTM "\trm -f \$(ALL) *.o\n";
print OUTM "\n";

print OUTM "EXTRA_CFLAGS += -DMODULE -DNO_RTL=1\n";
print OUTM "EXTRA_CFLAGS += -I\$(SUBDIRS)/../../include -I$::rcg_src_dir/src/include\n";
print OUTM "EXTRA_CFLAGS += -ffast-math -m80387 -msse2 -fno-builtin-sincos -mpreferred-stack-boundary=4\n";

print OUTM "obj-m += $::skeleton" . ".o\n";

print OUTM "\n";
close OUTM;
}
