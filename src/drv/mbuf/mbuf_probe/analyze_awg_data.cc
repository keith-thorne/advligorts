//
// Created by erik.vonreis on 4/29/21.
//

#include <iostream>
#include "shmem_awg.h"
#include "analyze_awg_data.hh"
#include "analyze_header.hh"


using namespace std;

namespace analyze
{

#define PRINTVAL(X) printf(#X " = %d\n", X);

    void dump_awg_data(volatile AWG_DATA *awg_data)
    {
        int hunt_state = 0;
        int slot = 0;

        auto detected_type = analyze_header(awg_data);
        if(detected_type != MBUF_AWG_DATA)
        {
            cout << "WARNING: attempting to analyze AWG_DATA structure, but structure was of type " << detected_type << endl;
        }

        PRINTVAL(_MAX_PAGE);
        PRINTVAL(AWG_EPOCHS_PER_SLOT);
        PRINTVAL(AWG_SLOTS);
        PRINTVAL(AWG_PAGE_COUNT);
        PRINTVAL(sizeof(AWG_DATA));

        while(true)
        {
            int page_index = FIND_PAGE_INDEX(slot,0);
            volatile AWG_DATA_PAGE * page = & awg_data->page[page_index];
            printf("slot: %1d status: %8x d: ", slot, page->status);
            for ( int i = 0; i < 4; ++i )
            {
               printf("%12.5g", page->buf[i]);
            }
            cout << "\r" << flush;

            // update slot if necessary to look for active slots
            switch(hunt_state)
            {
            case 1:
            case 0:
                if(page->status != 0)
                {
                    ++hunt_state;
                }
                else
                {
                    hunt_state = 0;
                }
                break;
            case 2:
                if(page->status != 0)
                {
                    slot = (++slot) % AWG_SLOTS;
                }
                else
                {
                    hunt_state = 0;
                }

            }

            usleep(20000);
        }
    }

    void analyze_awg_data( volatile void*    buffer,
                           std::size_t       size,
                           const ConfigOpts& options )
    {

        dump_awg_data(
            reinterpret_cast<volatile AWG_DATA *>(buffer)
            );
    }
}