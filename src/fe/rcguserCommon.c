/// @file rcguserCommon.c
/// @brief File contains routines for user app startup

#include "print_io_info.c"

// **********************************************************************************************
// Capture SIGHALT from ControlC
void
intHandler( int dummy )
{
    pLocalEpics->epicsInput.vmeReset = 1;
}

// **********************************************************************************************

void
attach_shared_memory( char* sysname )
{

    char         shm_name[ 64 ];
    extern char* addr;

    // epics shm used to communicate with model's EPICS process
    sprintf( shm_name, "%s", sysname );
    findSharedMemory( sysname );
    _epics_shm = (char*)addr;
    if ( _epics_shm < 0 )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _epics_shm );
        return -1;
    }
    printf( "EPICSM at 0x%lx\n", (long)_epics_shm );

    // testpoint config shm used to control testpoints from awgtpman
    sprintf( shm_name, "%s%s", sysname, SHMEM_TESTPOINT_SUFFIX );
    findSharedMemory( sysname );
    _tp_shm = (volatile TESTPOINT_CFG *)addr;
    if ( (uint64_t)_tp_shm < 0 )
    {
        printf( "mbuf_allocate_area(tp) failed; ret = %d\n", _tp_shm );
        return -1;
    }
    printf( "TPSM at 0x%lx\n", (long)_tp_shm );
    
    // awg data shm used to stream data from awgtpman
    sprintf( shm_name, "%s%s", sysname, SHMEM_AWG_SUFFIX );
    findSharedMemory( sysname );
    _awg_shm = (volatile AWG_DATA *)addr;
    if ( (uint64_t)_awg_shm < 0 )
    {
        printf( "mbuf_allocate_area(awg) failed; ret = %d\n", _awg_shm );
        return -1;
    }
    printf( "AWGSM at 0x%lx\n", (long)_awg_shm );
    
    // ipc_shm used to communicate with IOP
    findSharedMemory( "ipc" );
    _ipc_shm = (char*)addr;
    if ( _ipc_shm < 0 )
    {
        printf( "mbuf_allocate_area(ipc) failed; ret = %d\n", _ipc_shm );
        return -1;
    }
    printf( "IPC    at 0x%lx\n", (long)_ipc_shm );
    ioMemData = (volatile IO_MEM_DATA*)( ( (char*)_ipc_shm ) + 0x4000 );
    printf(
        "IOMEM  at 0x%lx size 0x%x\n", (long)ioMemData, sizeof( IO_MEM_DATA ) );
    printf( "%d PCI cards found\n", ioMemData->totalCards );

    // DAQ is via shared memory
    sprintf( shm_name, "%s_daq", sysname );
    findSharedMemory( shm_name );
    _daq_shm = (char*)addr;
    if ( _daq_shm < 0 )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _daq_shm );
        return -1;
    }
    printf( "DAQSM at 0x%lx\n", _daq_shm );
    daqPtr = (struct rmIpcStr*)_daq_shm;

    // shmipc is used to send SHMEM IPC data between processes on same computer
    findSharedMemory( "shmipc" );
    _shmipc_shm = (char*)addr;
    if ( _shmipc_shm < 0 )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _shmipc_shm );
        return -1;
    }

    // Open new IO shared memory in support of no hardware I/O
    findSharedMemory( "virtual_io_space" );
    _io_shm = (char*)addr;
    if ( _io_shm < 0 )
    {
        printf( "mbuf_allocate_area() failed; ret = %d\n", _io_shm );
        return -1;
    }
    ioMemDataIop = (volatile IO_MEM_DATA_IOP*)( ( (char*)_io_shm ) );
}

