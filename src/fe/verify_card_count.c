/// @file verify_card_count.c
/// @brief Helper file for moduleLoad.c to check prior IO card counts/types.

void initialize_card_counts( CDS_HARDWARE*, int[] );
int  verify_card_count( CDS_HARDWARE*, int[] );

void
initialize_card_counts( CDS_HARDWARE* pCds, int cards_per_model[] )
{
    int ii;

    pCds->adcCount = 0;
    pCds->dacCount = 0;
    pCds->dioCount = 0;
    pCds->doCount = 0;

    for ( ii = 0; ii < MAX_IO_MODULES; ii++ )
    {
        cards_per_model[ ii ] = 0;
        pCds->card_count[ ii ] = 0;
    }
    // Determine total number of cards model is looking for by type
    for ( ii = 0; ii < pCds->cards; ii++ )
    {
        cards_per_model[ pCds->cards_used[ ii ].type ]++;
        // Contec6464 DIO are split into 2 cards, so add another
        if ( pCds->cards_used[ ii ].type == CON_6464DIO )
            cards_per_model[ CON_6464DIO ]++;
    }
}

int
verify_card_count( CDS_HARDWARE* pCds, int model_cards[] )
{
    int  ii;
    int  errStat = 0;
    int  cardsFound = 0;

    for ( ii = 0; ii < CDS_CARD_TYPES; ii++ )
    {
        if ( pCds->card_count[ ii ] < model_cards[ ii ] && 
		ii != CON_1616DIO )
        {
            errStat = IO_CONFIG_ERROR;
#ifdef REQUIRE_IO_CNT
            printk( "" SYSTEM_NAME_STRING_LOWER
                    ": ERROR: Did not find correct number of %s cards \n",
                    _cdscardtypename[ ii ] );
            printk( "" SYSTEM_NAME_STRING_LOWER
                    ": ERROR: Expected %d and found %d - exiting\n",
                    model_cards[ ii ],
                    pCds->card_count[ ii ] );
#else
            printk( "" SYSTEM_NAME_STRING_LOWER
                    ": WARNING: Did not find correct number of %s cards \n",
                    _cdscardtypename[ ii ] );
            printk( "" SYSTEM_NAME_STRING_LOWER
                    ": WARNING: Expected %d and found %d \n",
                    model_cards[ ii ],
                    pCds->card_count[ ii ] );
#endif
        }
    }

    return errStat;
}
