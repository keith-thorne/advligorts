inline int adc_status_update( adcInfo_t* );

inline int
adc_status_update( adcInfo_t* adcinfo )
{
    int ii = 0;
    int jj = 0;
    int kk = 0;
    int status = 0;

    for ( jj = 0; jj < cdsPciModules.adcCount; jj++ )
    {
        kk = cdsPciModules.adcSlot[ jj ];
        pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_ADC_AIC;
        if ( pLocalEpics->epicsOutput.statAdc[ jj ] & ADC_MAPPED )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_MAPPED;
        if ( pLocalEpics->epicsOutput.statAdc[ jj ] & ADC_CAL_PASS )
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_AUTOCAL;
        // SET/CLR Channel Hopping Error
        if ( adcinfo->adcChanErr[ jj ] )
        {
            pLocalEpics->epicsOutput.statAdc[ jj ] &= ~( ADC_CHAN_HOP );
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_ADC_CHOP );
            status |= FE_ERROR_ADC;
        }
        else
        {
            pLocalEpics->epicsOutput.statAdc[ jj ] |= ADC_CHAN_HOP;
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_ADC_CHOP;
        }
        adcinfo->adcChanErr[ jj ] = 0;

        // Check ADC Timing
        if ( adcinfo->adcRdTimeErr[ jj ] )
        {
            pLocalEpics->epicsOutput.statAdc[ jj ] &= ~( ADC_RD_TIME );
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_TIMING );
        }
        else
        {
            pLocalEpics->epicsOutput.statAdc[ jj ] |= ADC_RD_TIME;
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_TIMING;
        }
        if ( adcinfo->adcRdTimeErr[ jj ] > MAX_ADC_WAIT_ERR_SEC )
            pLocalEpics->epicsOutput.stateWord |= FE_ERROR_ADC;
        adcinfo->adcRdTimeErr[ jj ] = 0;

        // SET/CLR Overflow Error
        if ( adcinfo->adcOF[ jj ] )
        {
            pLocalEpics->epicsOutput.statAdc[ jj ] &= ~( ADC_OVERFLOW );
            pLocalEpics->epicsOutput.statSlot[ kk ] &= ~( CARD_OVERRANGE );
            status |= FE_ERROR_OVERFLOW;
            ;
        }
        else
        {
            pLocalEpics->epicsOutput.statAdc[ jj ] |= ADC_OVERFLOW;
            pLocalEpics->epicsOutput.statSlot[ kk ] |= CARD_OVERRANGE;
        }
        adcinfo->adcOF[ jj ] = 0;
        for ( ii = 0; ii < 32; ii++ )
        {
            if ( pLocalEpics->epicsOutput.overflowAdcAcc[ jj ][ ii ] >
                 OVERFLOW_CNTR_LIMIT )
            {
                pLocalEpics->epicsOutput.overflowAdcAcc[ jj ][ ii ] = 0;
            }
            pLocalEpics->epicsOutput.overflowAdc[ jj ][ ii ] =
                adcinfo->overflowAdc[ jj ][ ii ];
            adcinfo->overflowAdc[ jj ][ ii ] = 0;
        }
    }
    return status;
}
