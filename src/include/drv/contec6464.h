#define C_DIO_6464L_PE  0x8682

int contec6464Init( CDS_HARDWARE* , struct pci_dev* );
unsigned int contec6464WriteOutputRegister( CDS_HARDWARE* , int, unsigned int );
unsigned int contec6464ReadOutputRegister( CDS_HARDWARE* , int );
unsigned int contec6464ReadInputRegister( CDS_HARDWARE*,  int );

