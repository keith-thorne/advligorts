/// \file plx_9056.c
/// \brief File contains common PLX chip DMA routines for ADC and DAC modules

#include "plx_9056.h"

// ADC DMA Functions
int plx9056_check_dma_done( int );
int plx9056_wait_dma_done( int, int, int );
void plx9056_adc_dma_setup( int, int );
void plx9056_adc_dma_enable( int );
void plx9056_adc_dma_start( int );

// DAC DMA Functions
void plx9056_dac_1820_dma_setup( int );
void plx9056_dac_16_dma_setup( int );
void plx9056_dac_dma_start( int );


// *****************************************************************************
/// \brief Function checks status of DMA DONE bit for an ADC module.
///     @param[in] module ID of ADC board to read.
/// @return Status of ADC module DMA DONE bit (0=not complete, 1= complete)
// *****************************************************************************
int
plx9056_check_dma_done( int module )
{
    // Return 0 if DMA not complete
    if ( ( adcDma[ module ]->DMA_CSR & PLX_DMA_DONE ) == 0 )
        return ( 0 );
    // Return 1 if DMA is complete
    else
        return ( 1 );
}

// *****************************************************************************
/// \brief Function if DMA from ADC module is complete.
///< Code will remain in loop until DMA is complete.
///     @param[in] module ID of ADC board to read.
///     @param[in] Time, in usec, to wait between checks.
///     @param[in]  Number of checks before timeout.
/// @param[out] data Status of DMA DONE bit.
/// @return ADC DMA Status (-1= timeout, 0=complete
/// Note: This function not presently used.
// *****************************************************************************
int
plx9056_wait_dma_done( int module, int timeinterval, int time2wait)
{
    int ii = 0;
    do
    {
        ii++;
        udelay( timeinterval );
    } while ( ( adcDma[ module ]->DMA_CSR & PLX_DMA_DONE == 0 ) &&
              ( ii < time2wait ) );
    // If DMA did not complete, return error
    if ( ii >= time2wait )
        return -1;
    else
        return 0;
}

// *****************************************************************************
/// \brief Function performs DMA setup for ADC cards.
/// Called only once at startup
///     @param[in] module ID of ADC board.
///     @param[in] Number of bytes to be transferred.
// *****************************************************************************
void
plx9056_adc_dma_setup( int module, int dma_bytes )
{
    /// Set DMA mode such that completion does not cause interrupt on bus.
    adcDma[ module ]->DMA0_MODE = PLX_DMA_MODE_NO_INTR;
    /// Load PCI address (remapped local memory) to which data is to be
    /// delivered.
    adcDma[ module ]->DMA0_PCI_ADD = (int)adc_dma_handle[ module ];
    /// Set the PCI address of board where data will be transferred from.
    adcDma[ module ]->DMA0_LOC_ADD = PLX_DMA_LOCAL_ADDR;
    /// Set the number of bytes to be transferred.
    adcDma[ module ]->DMA0_BTC = dma_bytes;
    /// Set the DMA direction ie ADC to computer memory.
    adcDma[ module ]->DMA0_DESC = PLX_DMA_TO_PCI;
}

// *****************************************************************************
/// \brief Function sets up starts Demand DMA for ADC modules.
/// Called once on startup.
///     @param[in] module ID of ADC board.
// *****************************************************************************
void
plx9056_adc_dma_enable( int module )
{
    /// Set DMA mode and direction in PLX controller chip on module.
    adcDma[ module ]->DMA0_MODE = PLX_DMA_MODE_NO_INTR | PLX_DEMAND_DMA;
    /// Enable DMA
    adcDma[ module ]->DMA_CSR = PLX_DMA_START;
}

// *****************************************************************************
/// \brief Function starts DMA for ADC modules.
/// Called at end of every ADC read to arm ADC DMA for next read.
///     @param[in] module ID of ADC board.
// *****************************************************************************
void
plx9056_adc_dma_start( int module )
{
    adcDma[ module ]->DMA_CSR = PLX_DMA_START;
}

// *****************************************************************************
// Following are DAC DMA Funtions ********************************************
// *****************************************************************************

// *****************************************************************************
/// \brief Function sets up DMA for 18 and 20bit DAC modules.
/// Called once on startup.
///     @param[in] module ID of DAC board.
// *****************************************************************************
void
plx9056_dac_1820_dma_setup( int modNum )
{
    // dacDma[ modNum ]->DMA1_MODE = GSAI_DMA_MODE_NO_INTR;
    dacDma[ modNum ]->DMA1_MODE = PLX_DMA_MODE_NO_INTR;
    dacDma[ modNum ]->DMA1_PCI_ADD = (int)dac_dma_handle[ modNum ];
    dacDma[ modNum ]->DMA1_LOC_ADD = GSAO1820_OUTBUF_LOCAL_ADDRESS;
#ifdef OVERSAMPLE_DAC
    dacDma[ modNum ]->DMA1_BTC = GSAO1820_BTC * OVERSAMPLE_TIMES;
#else
    dacDma[ modNum ]->DMA1_BTC = GSAO1820_BTC;
#endif
    dacDma[ modNum ]->DMA1_DESC = PLX_DMA_FROM_PCI;

}

// *****************************************************************************
/// \brief Function sets up DMA for 16bit DAC modules.
/// Called once on startup.
///     @param[in] module ID of DAC board.
// *****************************************************************************
void
plx9056_dac_16_dma_setup( int modNum )
{
    dacDma[ modNum ]->DMA1_MODE = PLX_DMA_MODE_NO_INTR;
    dacDma[ modNum ]->DMA1_PCI_ADD = (int)dac_dma_handle[ modNum ];
    dacDma[ modNum ]->DMA1_LOC_ADD = GSAO16_OUTBUF_LOCAL_ADDRESS;
#ifdef OVERSAMPLE_DAC
    dacDma[ modNum ]->DMA1_BTC = GSAO16_BTC * OVERSAMPLE_TIMES;
#else
    dacDma[ modNum ]->DMA1_BTC = GSAO16_BTC;
#endif
    dacDma[ modNum ]->DMA1_DESC = PLX_DMA_FROM_PCI;
}

// *****************************************************************************
/// \brief Function starts DMA for ADC modules.
/// Called at end of every ADC read to arm ADC DMA for next read.
///     @param[in] module ID of DAC board.
// *****************************************************************************
void
plx9056_dac_dma_start( int modNum )
{
    dacDma[ modNum ]->DMA_CSR = PLX_DMA1_START;

}
