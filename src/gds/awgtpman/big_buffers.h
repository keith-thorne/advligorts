//
// Created by erik.vonreis on 5/5/21.
//

#ifndef DAQD_TRUNK_BIG_BUFFERS_H
#define DAQD_TRUNK_BIG_BUFFERS_H

#include <semaphore.h>
#include "dtt/awgtype.h"

#define _MAX_BUF		DAQ_NUM_DATA_BLOCKS

// for linking purposes, we move the big buffers out into their own object file

struct streambufpage_t {
    /* ready flag: 0 - not ready, 1 - ready, 2 - in process */
    int		ready;
    /* Time stamp of last fill */
    taisec_t		time;
    int		epoch;
    /* page length */
    int		pagelen;
    /* Data buffer */
    float		buf[_MAX_PAGE];
};
typedef struct streambufpage_t streambufpage_t;

struct streambuf_t {
    /* stream buffer pages */
    streambufpage_t	buf[NUMBER_OF_EPOCHS * MAX_STREAM_BUFFER];
};
typedef struct streambuf_t streambuf_t;

struct awgpagebuf_t {
    /* ouput type */
    AWG_OutputType	otype;
    /* output pointer (testpoints) */
    float*		optr;
    /* page index into shared memory */
    int             page_index;
    /* channel number (DAC) */
    int		onum;
    /* page length */
    int		pagelen;
    /* status of page buffer */
    int		status;

    uint32_t        _reserved[2]; //must be aligned to 16 bytes
    /* buffer for page */
    float		page [_MAX_PAGE];
};
typedef struct awgpagebuf_t awgpagebuf_t;

struct awgbuf_t {
    /* ready flag: 0 - not ready, 1 - ready, 2 - in process */
    int		ready;
    /* time and epoch described with this buffer */
    taisec_t		time;
    int		epoch;
    /* buffer for waveforms */
    awgpagebuf_t	buf[MAX_NUM_AWG];
};
typedef struct awgbuf_t awgbuf_t;


struct awgmbuf_t {
    /* counts how many buffers are ready */
    sem_t		ready;
    /* buffers for awgs */
    awgbuf_t		bufs[_MAX_BUF];
};
typedef struct awgmbuf_t awgmbuf_t;

extern awgmbuf_t		awgbuf;
extern streambuf_t	 	awgstream[MAX_NUM_AWG];

#endif // DAQD_TRUNK_BIG_BUFFERS_H
