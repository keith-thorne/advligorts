static char *versionId = "Version $Id$" ;
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: rmapi							*/
/*                                                         		*/
/* Module Description: implements functions for accessing the RM	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _RM_RANGE_CHECK 
#define _RM_RANGE_CHECK
#endif

/* Header File List: */
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <semaphore.h>
#include <errno.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "dtt/gdsutil.h"
#include "dtt/gdstask.h"
#include "dtt/hardware.h"
#include "dtt/rmapi.h"
#include <unistd.h>
#include <sys/ioctl.h>

#include "../drv/mbuf/mbuf.h"



#define RMAPI_MAXNODE		4

#define VMIC5588_RAM_OFFSET	0x40
#define DMA_THRESHOLD		256*1000000


   static char*		rm[] = {
   (char*) (0),
   (char*) (0),
   (char*) (0),
   (char*) (0)};

   static char*		rmboard[] = {
   (char*) (0),
   (char*) (0),
   (char*) (0),
   (char*) (0)};

   static int		rmsize[] = {0, 0, 0, 0};
   static int 		rmmaster[] = {0, 0, 0, 0};

   struct rmISR_ctrl {
      short		id;
      int		inum;
      int 		level;
      int		vec;
      rmISR		isr;
      void*		arg;
      int		status;
   };
   typedef struct rmISR_ctrl rmISR_ctrl;
   static rmISR_ctrl	rmIntCtrl[RMAPI_MAXNODE][4];
   static rmISR_ctrl*	ctrlISR = NULL;
   static sem_t		semISR;
   static taskID_t	tidISR;
   static int		badData = 0;
   static int		badFIFO = 0;

   char* rmBaseAddress (short ID)
   {
      switch (ID) {
         case 0:
         case 1:
         case 2:
         case 3:
            {
               return rm[ID];
            }
         default:
            {
               return NULL;
            }
      }
   }


   char* rmBoardAddress (short ID)
   {
      switch (ID) {
         case 0:
         case 1:
         case 2:
         case 3:
            {
               return rmboard[ID];
            }
         default:
            {
               return NULL;
            }
      }
   }


   int rmBoardSize (short ID)
   {
      switch (ID) {
         case 0:
         case 1:
         case 2:
         case 3:
            {
               return rmsize[ID];
            }
         default:
            {
               return 0;
            }
      }
   }

/*
  Handels interrupts 1,2,3
  records sender and interrupt number
  gives interrupt semaphore
*/
   int rmClearInt (short ID, int* sid, int* inum)
   {
      volatile char* volatile	addr;	/* base address */
   
      addr = rmBoardAddress (ID);
      if (addr == NULL) {
         return -1;
      }
   
      /* toggle the damn led */
   #ifdef DEBUG
      ((RMREGS*) addr)->csr ^= 0x80;
   #endif
   
      *sid = ((RMREGS*) addr)->sid1;
      *inum = 0x0007 & ((RMREGS*) addr)->irs;
   
      return 0;
   }

/*
  Turns the FAIL led on or off
  setting the FAIL led will have no effect on the
  operation of the 5588 board.  The 5588 does not
  set or clear the FAIL led under any circumstances.
*/
   int rmLED (short ID, int led_on)
   {
      volatile char* volatile	addr;	/* base address */
   
      /* get base address */
      addr = rmBoardAddress (ID);
      if (addr == NULL) {
         return -1;
      }
      /* set LED */
      if (led_on) {
         ((RMREGS*) addr)->csr |= 0x80;
      }
      else {
         ((RMREGS*) addr)->csr &= ~(0x80);
      }
   
      return 0;
   }

/*
  Bypasses the current Reflected memory Node
  must have external optical switch installed
*/
   int rmNodeBypass (short ID, int bypass)
   {
      volatile char* volatile	addr;	/* base address */
   
      /* get base address */
      addr = rmBoardAddress (ID);
      if (addr == NULL) {
         return -1;
      }
      /* set bypass */
      if (bypass) {
         ((RMREGS*) addr)->irs |= (0x08);
      }
      else {
         ((RMREGS*) addr)->irs &= ~(0x08);
      }
   
      return 0;
   }


int mbuf_opened = 0;
int mbuf_fd = 0;

//   int rmInit (short ID)
//   {
//      // 0 -- our shared memory
//      // 2 -- IPC shared memory
//      if (ID == 0 || ID == 2) {
//	 int fd;
//	 void *addr;
//         char fname[128];
//	 extern char system_name[PARAM_ENTRY_LEN];
//
//         /*rmboard[ID] = rm[ID] = malloc (rmsize[ID]);*/
//         rmmaster[ID] = 0;
//
//	 strcpy(fname, "/dev/mbuf");
//	 if (!mbuf_opened) {
//	   if ((fd = open (fname, O_RDWR | O_SYNC)) < 0) {
//	       fprintf(stderr, "Couldn't open /dev/mbuf read/write\n");
//	       _exit(-1);
//	   }
//	   mbuf_opened = 1;
// 	   mbuf_fd = fd;
//	 } else fd = mbuf_fd;
//	 struct mbuf_request_struct req;
//	 if (ID == 0) {
//		strcpy(req.name, system_name);
//		rmsize[ID] =  64 * 1024 * 1024;
//	 } else {
//		strcpy(req.name, "ipc");
//		rmsize[ID] =  4 * 1024 * 1024;
//	 }
//	 req.size = rmsize[ID];
//         ioctl (fd, IOCTL_MBUF_ALLOCATE, &req);
//         //ioctl (fd, IOCTL_MBUF_INFO, &req);
//
//         addr = mmap(0, rmsize[ID], PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
//         if (addr == MAP_FAILED) {
//                printf("return was %d\n",errno);
//                perror("mmap");
//                _exit(-1);
//         }
//         printf("%s mmapped address is 0x%lx\n", fname, (long)addr);
//         rmboard[ID] = rm[ID] = addr;
//      }
//      else if (ID == 1) {
//         rm[ID] = NULL;
//         rmboard[ID] = NULL;
//         rmsize[ID] = 0;
//         rmmaster[ID] = 0;
//      }
//      else {
//         printf ("rmInit: Illegal board ID\n");
//         return -1;
//      }
//   #ifdef DEBUG
//      printf("RM%i: Node ID: %i -- Shared memory (%s)\n",
//            ID, rm[ID] ? "/gds/" : "");
//   #endif
//      return 0;
//   }




//   void initReflectiveMemory (void)
//   {
//      memset (rmIntCtrl, 0, sizeof (rmIntCtrl));
//      tidISR = 0;
//      sem_init (&semISR, 0, 0);
//      rmInit (0);
//      rmInit (1);
//
//      //// Open the IPC memory space
//      //rmInit (2);
//
//   }

/*
  generate and interrupt on a node
  num = interrupt number
  node = 0..255 or RMAPI_NODE_ALL
*/
   int rmInt (short ID, int inum, int node)
   {
      volatile char* volatile	addr;	/* base address */
   
      /* get base address */
      addr = rmBoardAddress (ID);
      if (addr == NULL) {
         return -1;
      }
   
   #ifdef DEBUG
      if ((inum < 0) || (inum > 3)) {
         printf ("RMAPI: Invalid Interrupt Number\n");
         return -2;
      }
   
      if (((node < 0) || (node > 255)) && (node != RMAPI_NODE_ALL)) {
         printf ("RMAPI: Invalid Node ID\n");
         return -2;
      }
   #endif
   
      /* Issue Interrupt */
      if (node == RMAPI_NODE_ALL) {
         ((RMREGS*) addr)->cmd = (0x43 & inum);
      } 
      else {
         ((RMREGS*) addr)->cmdn = node;
         ((RMREGS*) addr)->cmd = (0x03 & inum);
      }
   
      return 0;
   }

/*
  Resets the given node
  node = 0..255 or RMAPI_NODE_ALL
*/
   int rmResetNode (short ID, int node)
   {
      volatile char* volatile	addr;	/* base address */
   
      /* get base address */
      addr = rmBoardAddress (ID);
      if (addr == NULL) {
         return -1;
      }
   
   #ifdef DEBUG
      if ((node < 0 || node > 255) && node != RMAPI_NODE_ALL) {
         printf("RMAPI: Invalid Node ID\n");
         return -2;
      }
   #endif
   
      /* Issue reset */
      if (node == RMAPI_NODE_ALL) {
         ((RMREGS*) addr)->cmd = (0x40);
      } 
      else {
         ((RMREGS*) addr)->cmdn = node;
         ((RMREGS*) addr)->cmd = (0x00);
      }
      return 0;
   }


/*
  Checks the validity of a reflective memory address range
  NOTE: offset must also be 4-byte aligned 
*/

   int rmCheck (short ID, int offset, int size) 
   {
      return 1;
   }


/*
  Installs an interrupt service routine
*/
   int rmIntInstall (short ID, int inum, rmISR isr, void* arg)
   {
      return -10;
   }


/*
  interrupt service routine for watch
*/
   static void watchInt (short ID, int inum, int cause, void* arg)
   {
      char		buf[1024];
      sprintf (buf, "RM interrupt (%i) by board %i: ", inum, ID);
      if (inum == 0) {
         sprintf (strend (buf), "status 0x%x ", cause);
         if ((cause & 0x10) != 0) {
            sprintf (strend (buf), "(data bad %i, ", badData);
         }
         else {
            sprintf (strend (buf), "(data ok, ");
         }
         if ((cause & 0x01) != 0) {
            sprintf (strend (buf), "FIFO bad %i)\n", badFIFO);
         }
         else {
            sprintf (strend (buf), "FIFO ok)\n");
         }
      }
      else {
         sprintf (strend (buf), "node %i\n", cause);
      }
      printf ("%s", buf);
      gdsWarningMessage (buf);
   }

/*
  Installs an interrupt service routine
*/
   int rmWatchInt (short ID, int inum)
   {
      if (inum >= 0) {
         return rmIntInstall (ID, inum, watchInt, NULL);
      }
      else {
         return rmIntInstall (ID, 0, watchInt, NULL) +
            rmIntInstall (ID, 1, watchInt, NULL) +
            rmIntInstall (ID, 2, watchInt, NULL) +
            rmIntInstall (ID, 3, watchInt, NULL);
      }
   }


/*
  Copies from Reflected memory to local memory
  using Baja as DMA controller
  pData = pointer to local memory buffer
  offset = offset into reflected to read
  size = number of bytes to read

  NOTE: offset must be 4-byte aligned 
*/
   int rmRead (short ID, char* pData, int offset, int size, int flag)
   {
      int 		status;
      volatile char* volatile	addr;	/* base address */
   
   #ifdef _RM_RANGE_CHECK
      if (!rmCheck (ID, offset, size)) {
         return -2;
      }
   #endif
   
      /* printf ("rmRead (%i) at ofs=0x%x, size=%i\n", ID, offset, size); */
   
      /* get base address */
      addr = rmBaseAddress (ID);
      if (addr == NULL) {
         return -1;
      }
   
      memcpy ((void*) pData, (void*) (addr + offset), size);
      status = 0;

   #ifdef DEBUG
      if(status != 0)
         gdsDebug ("RMAPI: Error sysVicBlkCopy");
   #endif
   
      return status;
   }


/*
  Copies to Reflected memory from local memory
  using Baja as DMA controller
  pData = pointer to local memory buffer
  offset = offset into reflected to read
  size = number of bytes to read

  NOTE: offset must be 4-byte aligned 
*/
   int rmWrite (short ID, char* pData, int offset, int size, int flag)
   {
      int status;
      volatile char* volatile	addr;	/* base address */
   
   #ifdef _RM_RANGE_CHECK
      if (!rmCheck (ID, offset, size)) {
         return -2;
      }
   #endif
   
      /*printf ("rmWrite (%i) at ofs=0x%x, size=%i\n", ID, offset, size); */
   
      /* get base address */
      addr = rmBaseAddress (ID);
      if (addr == NULL) {
         return -1;
      }
   
      memcpy ((void*) (addr + offset), (void*) pData, size);
      status =0;
   #ifdef DEBUG
      if(status != 0)
         gdsDebug ("RMAPI: Error  sysVicBlkCopy.\n");
   #endif
   
      return status;
   }

