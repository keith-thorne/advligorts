package main

import (
	"reflect"
	"testing"
)

func Test_parseIntList(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name      string
		args      args
		wantError bool
		results   []uint
	}{
		{
			name:      "empty",
			args:      args{""},
			wantError: false,
			results:   []uint{},
		},
		{
			name:      "one item",
			args:      args{"5"},
			wantError: false,
			results:   []uint{5},
		},
		{
			name:      "multiple items",
			args:      args{"5,42,99"},
			wantError: false,
			results:   []uint{5, 42, 99},
		},
		{
			name:      "invalid items",
			args:      args{"5-42,99"},
			wantError: true,
			results:   []uint{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got1, got := ParseIntList(tt.args.input)
			gotError := got != nil
			if gotError != tt.wantError {
				t.Errorf("ParseIntList() got = %v, wantError %v", got, tt.wantError)
			}
			if !reflect.DeepEqual(got1, tt.results) {
				t.Errorf("ParseIntList() got1 = %v, wantError %v", got1, tt.results)
			}
		})
	}
}

func Test_parseTPList(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name      string
		args      args
		wantError bool
		want1     []TPReq
	}{
		{
			name:      "empty",
			args:      args{""},
			wantError: false,
			want1:     []TPReq{},
		},
		{
			name:      "one item",
			args:      args{"5:1"},
			wantError: false,
			want1:     []TPReq{{5, 1}},
		},
		{
			name:      "multiple items",
			args:      args{"5:1,5:2,99:1"},
			wantError: false,
			want1:     []TPReq{{5, 1}, {5, 2}, {99, 1}},
		},
		{
			name:      "invalid items",
			args:      args{"5-42,99"},
			wantError: true,
			want1:     []TPReq{},
		},
		{
			name:      "invalid tp",
			args:      args{"5:42,99:-1"},
			wantError: true,
			want1:     []TPReq{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got1, got := ParseTPList(tt.args.input)
			gotError := got != nil
			if gotError != tt.wantError {
				t.Errorf("parseTPList() got = %v, want %v", got, tt.wantError)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("parseTPList() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_getNDcus(t *testing.T) {
	type args struct {
		count int
	}
	tests := []struct {
		name string
		args args
		want []uint
	}{
		{
			name: "empty",
			args: args{0},
			want: []uint{},
		},
		{
			name: "one dcu",
			args: args{1},
			want: []uint{5},
		},
		{
			name: "5 dcus",
			args: args{5},
			want: []uint{5, 6, 7, 8, 9},
		},
		{
			name: "10 dcus",
			args: args{10},
			want: []uint{5, 6, 7, 8, 9, 10, 11, 12, 17, 18},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getNDcus(tt.args.count); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getNDcus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_toStringList(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "empty",
			args: args{
				input: "",
			},
			want: []string{},
		},
		{
			name: "single entry",
			args: args{
				input: "abc",
			},
			want: []string{"abc"},
		},
		{
			name: "single entry with a leading space",
			args: args{
				input: " abc",
			},
			want: []string{"abc"},
		},
		{
			name: "single entry with leading spaces",
			args: args{
				input: "    abc",
			},
			want: []string{"abc"},
		},
		{
			name: "single entry with a trailing space",
			args: args{
				input: "abc ",
			},
			want: []string{"abc"},
		},
		{
			name: "single entry with trailing spaces",
			args: args{
				input: "abc   ",
			},
			want: []string{"abc"},
		},
		{
			name: "single entry with leading and trailing spaces",
			args: args{
				input: "  abc   ",
			},
			want: []string{"abc"},
		},
		{
			name: "multi entries",
			args: args{
				input: " abc def   ghi  jkl  mno pqr stu  ",
			},
			want: []string{"abc", "def", "ghi", "jkl", "mno", "pqr", "stu"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := toStringList(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("toStringList() = %v, want %v", got, tt.want)
			}
		})
	}
}
