package main

import (
	"errors"
	"strconv"
	"strings"
)

func ParseIntList(input string) ([]uint, error) {
	var err error
	var val uint64
	results := make([]uint, 0)

	if input != "" {
		for _, segment := range strings.Split(input, ",") {
			val, err = strconv.ParseUint(segment, 10, 32)
			if err == nil {
				results = append(results, uint(val))
			} else {
				results = []uint{}
				break
			}
		}
	}
	return results, err
}

type TPReq struct {
	DCUId uint
	TP    uint
}

func parseTPReq(input string) (TPReq, error) {
	var err error
	var val uint64
	var result TPReq
	parts := strings.Split(input, ":")
	if len(parts) != 2 {
		return TPReq{}, errors.New("Invalid TP request")
	}
	val, err = strconv.ParseUint(parts[0], 10, 32)
	if err != nil {
		return TPReq{}, err
	}
	result.DCUId = uint(val)
	val, err = strconv.ParseUint(parts[1], 10, 32)
	if err != nil {
		return TPReq{}, err
	}
	result.TP = uint(val)
	return result, nil
}

func ParseTPList(input string) ([]TPReq, error) {
	var err error
	var val TPReq
	results := make([]TPReq, 0)

	if input != "" {
		for _, segment := range strings.Split(input, ",") {
			val, err = parseTPReq(segment)
			if err == nil {
				results = append(results, val)
			} else {
				results = []TPReq{}
				break
			}
		}
	}
	return results, err
}

func getNDcus(count int) []uint {
	dcus := make([]uint, 0, 256)
	for i := uint(5); i <= 12; i++ {
		dcus = append(dcus, i)
	}
	for i := uint(17); i < 256; i++ {
		dcus = append(dcus, i)
	}
	if count < 0 || count > len(dcus) {
		dcus = dcus[0:0]
	} else {
		dcus = dcus[0:count]
	}
	return dcus
}

func toStringList(input string) []string {
	output := strings.Split(input, " ")
	for i := 0; i < len(output); {
		if output[i] == "" {
			output = append(output[:i], output[i+1:]...)
		} else {
			i++
		}
	}
	return output
}
