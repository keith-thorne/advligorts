package fe_simulation

//#include <stdint.h>
//#include "shmem_all.h"
import "C"
import (
	"io"
	"unsafe"
)

// move data from the awg buffer for a model
type AwgGenerator struct {
	model 		*Model
	slot		int
}

func (g *AwgGenerator)Name() string {
	return "AwgGenerator"
}

func (g *AwgGenerator)FullChannelName() string {
	return "Unknown"
}

func (g *AwgGenerator)BytesPerSec() RateBytes {
	return g.model.DataRate
}

func CalcPageIndex(slot, epoch int) int {
	return slot * C.AWG_EPOCHS_PER_SLOT + epoch % C.AWG_EPOCHS_PER_SLOT
}

func (g *AwgGenerator)Generate(gpsSec, gpsNano int, out uintptr) uintptr {
	rate := g.model.ModelRate.Rate / 16
	epoch := int( gpsNano / (1e9/16) )
	page_index := CalcPageIndex(g.slot, epoch)
	out_data, end := int32Slice(out, rate)

	if(g.model.AWG_data.page[page_index].status == 0) {
		awg_data, _ := int32Slice(uintptr(unsafe.Pointer(&g.model.AWG_data.page[page_index].buf)), rate)
		copy(out_data, awg_data)
	} else {
		for i := range out_data {
			out_data[i] = 0
		}
	}

	return end
}

func (g *AwgGenerator)OutputIniEntry(writer io.Writer) error {
	return nil
}

func (g *AwgGenerator) OutputParEntry(writer io.Writer) error {
	return nil
}

func (g *AwgGenerator) DataType() int {
	return dataTypeFloat32
}

func (g *AwgGenerator) DataRate() RateHertz {
	return g.model.ModelRate
}

func NewAwgGenerator(model *Model, slot int) *AwgGenerator {
	return &AwgGenerator{
		model: model,
		slot: slot,
	}
}