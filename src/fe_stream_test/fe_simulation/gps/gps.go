package gps

/*
#include <stdint.h>
#include <sys/ioctl.h>

#define SYMMETRICOM_TIME 1

static void gps_now(int fd, int64_t *dest_sec, int32_t *dest_nano) {
	unsigned long t[3];
	ioctl( fd, SYMMETRICOM_TIME, &t);
	t[1] *= 1000;
	t[1] += t[2];
	*dest_sec = t[0];
	*dest_nano = t[1];
}

*/
import "C"
import (
	"os"
	"time"
)

const ioctlStatus = 0
const ioctlTime = 1

type Clock struct {
	gpsFile *os.File
	gpsFd   uintptr
	offset  int64
}

func NewClock(offset int64) (*Clock, error) {
	gpsFile, err := os.Open("/dev/gpstime")
	if err != nil {
		return nil, err
	}
	return &Clock{
		gpsFile: gpsFile,
		gpsFd:   gpsFile.Fd(),
		offset:  offset,
	}, nil
}

func (c *Clock) Close() error {
	if c != nil {
		c.gpsFile.Close()
	}
	return nil
}

func (c *Clock) Now() time.Time {
	var gpsSec C.int64_t
	var gpsNano C.int32_t

	C.gps_now(C.int(c.gpsFd), &gpsSec, &gpsNano)
	return time.Unix(int64(gpsSec+C.int64_t(c.offset)), int64(gpsNano))
}
