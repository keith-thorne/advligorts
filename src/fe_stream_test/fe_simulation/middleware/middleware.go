package middleware

import (
	"fmt"
	"net/http"
	"time"
)

type statusRecorder struct {
	writer http.ResponseWriter
	status int
	start  time.Time
}

func (s *statusRecorder) Header() http.Header {
	return s.writer.Header()
}

func (s *statusRecorder) Write(bytes []byte) (int, error) {
	s.WriteHeader(http.StatusOK)
	return s.writer.Write(bytes)
}

func (s *statusRecorder) WriteHeader(statusCode int) {
	if s.status == 0 {
		s.status = statusCode
		s.writer.WriteHeader(statusCode)
	}
}

func (s *statusRecorder) GetStatus() int {
	s.WriteHeader(http.StatusOK)
	return s.status
}

func LoggingHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		recordingW := &statusRecorder{
			writer: w,
			status: 0,
			start:  time.Now(),
		}
		next(recordingW, r)
		duration := time.Now().Sub(recordingW.start)
		fmt.Printf("%s [%d] %v\n", r.URL.Path, recordingW.GetStatus(), duration)
	}
}

type Generator func(http.HandlerFunc) http.HandlerFunc

type GeneratorChain []Generator

func Chain(generators ...Generator) GeneratorChain {
	return generators
}

func ExtendedChain(base GeneratorChain, generators ...Generator) GeneratorChain {
	finalSet := make([]Generator, 0, len(base)+len(generators))
	finalSet = append(finalSet, base...)
	return append(finalSet, generators...)
}

func (g GeneratorChain) Apply(handler http.HandlerFunc) http.HandlerFunc {
	f := handler
	for i := len(g) - 1; i >= 0; i-- {
		f = g[i](f)
	}
	return f
}
