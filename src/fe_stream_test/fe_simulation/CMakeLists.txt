
if (GOLANG)
    message("Found go at ${GOLANG}")

    # The go program needs to link with C code and pull in C definitions.
    # To pass this information into go, w/o making the code text templates
    # for configure_file to expand, we will make a pkg-config file that has
    # the required information, and template that.
    #
    # Currently this requires knowledge of the extra bits that are required.
    # driver::shmem and access to the daqmap/daq_core/... headers
    get_target_property(FE_SIM_INCL driver::shmem INCLUDE_DIRECTORIES)
    get_target_property(FE_SIM_FLAGS driver::shmem COMPILE_FLAGS)
    get_target_property(FE_SIM_LINK_FLAGS driver::shmem LINK_FLAGS)
    get_target_property(FE_SIM_LINK_DIRS driver::shmem LINK_DIRECTORIES)
    get_target_property(FE_SIM_LINK_LIBS driver::shmem LINK_LIBRARIES)
    get_target_property(FE_SIM_LINK_LIB driver::shmem LIBRARY_OUTPUT_NAME)
    get_target_property(FE_SIM_LINK_OPTS driver::shmem LINK_OPTIONS)
    message("driver::shmem has the following include directories ${FE_SIM_INCL}")
    message("driver::shmem has the following compile flags ${FE_SIM_FLAGS}")
    message("driver::shmem has the following link flags ${FE_SIM_LINK_FLAGS}")
    message("driver::shmem has the following link dirs ${FE_SIM_LINK_DIRS}")
    message("driver::shmem has the following link libs ${FE_SIM_LINK_LIBS}")
    message("driver::shmem has the following link lib ${FE_SIM_LINK_LIB}")
    message("driver::shmem has the following link opts ${FE_SIM_LINK_OPTS}")

    SET(FE_SIM_INCL ${FE_SIM_INCL} ${CMAKE_SOURCE_DIR}/src/shmem)

    set(FE_SIM_LINK_LIBS_FINAL "")
    foreach (fems_cur_lib ${FE_SIM_LINK_LIBS})
        list(APPEND FE_SIM_LINK_LIBS_FINAL "${fems_cur_lib}")
        if (TARGET ${fems_cur_lib})
            get_target_property(fems_cur_incl "${fems_cur_lib}" INCLUDE_DIRECTORIES)
            get_target_property(fems_cur_req_libs "${fems_cur_lib}" LINK_LIBRARIES)
            if (fems_cur_req_libs)
                list(APPEND FE_SIM_LINK_LIBS_FINAL ${fems_cur_req_libs})
            endif (fems_cur_req_libs)
            list(APPEND FE_SIM_INCL "${fems_cur_incl}")
        endif (TARGET ${fems_cur_lib})
    endforeach(fems_cur_lib)

    message("FE_SIM_LINK_LIBS_FINAL = ${FE_SIM_LINK_LIBS_FINAL}")

    set(_SIM_STREAM_LIBS_ "-L${CMAKE_BINARY_DIR}/src/drv -lshmem")
    set(_SIM_STREAM_CFLAGS_ "-g3 -O")
    foreach(CUR_LIB ${FE_SIM_LINK_LIBS_FINAL})
        set(_SIM_STREAM_LIBS_ "${_SIM_STREAM_LIBS_} -l${CUR_LIB}")
    endforeach(CUR_LIB ${FE_SIM_LINK_LIBS_FINAL})
    foreach(CUR_INCL ${FE_SIM_INCL})
        set(_SIM_STREAM_CFLAGS_ "${_SIM_STREAM_CFLAGS_} -I${CUR_INCL}")
    endforeach(CUR_INCL ${FE_SIM_INCL})

    configure_file(fe_simulation.pc.in ${CMAKE_CURRENT_BINARY_DIR}/fe_simulation.pc @ONLY)

    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/fe_simulated_streams
            COMMAND ${CMAKE_COMMAND} -E env "PKG_CONFIG_PATH=${CMAKE_CURRENT_BINARY_DIR}:$PKG_CONFIG_DIR" ${GOLANG} build -o ${CMAKE_CURRENT_BINARY_DIR} git.ligo.org/cds/advligorts/src/fe_stream_test/fe_simulation/cmd/fe_simulated_streams
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            DEPENDS driver::shmem go.mod
            generator.go
            awg_generator.go
            ini_manager.go
            model.go
            simulation.go
            slice_tricks.go
            gps/gps.go
            middleware/middleware.go
            middleware/mux.go
            cmd/fe_simulated_streams/admin_interface.go
            cmd/fe_simulated_streams/fe_simulated_streams.go
            cmd/fe_simulated_streams/util.go
            ligo_crc/ligo_crc.go
            )

    add_custom_target(fe_simulated_streams ALL
            DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/fe_simulated_streams
            SOURCES
            go.mod
            generator.go
            awg_generator.go
            ini_manager.go
            model.go
            simulation.go
            slice_tricks.go
            gps/gps.go
            cmd/fe_simulated_streams/admin_interface.go
            cmd/fe_simulated_streams/fe_simulated_streams.go
            cmd/fe_simulated_streams/util.go
            ligo_crc/ligo_crc.go)

    install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/fe_simulated_streams
	    DESTINATION bin)
endif(GOLANG)
