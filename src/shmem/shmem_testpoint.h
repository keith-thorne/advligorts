//
// Created by erik.vonreis on 5/19/21.
//

#ifndef DAQD_TRUNK_SHMEM_TESTPOINT_H
#define DAQD_TRUNK_SHMEM_TESTPOINT_H

#include "testpoint_data.h"

// shared memory used by awg to control test points
// it's overloaded with some other uses, such as controlling awg slots.
#define SHMEM_TESTPOINT_SUFFIX "_tp"
#define SHMEM_TESTPOINT_SIZE_MB 1
#define SHMEM_TESTPOINT_SIZE (SHMEM_AWG_SIZE_MB * 1024 * 1024 )

#define SHMEM_TESTPOINT_STRUCT TESTPOINT_CFG

#endif // DAQD_TRUNK_SHMEM_TESTPOINT_H
