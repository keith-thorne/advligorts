//
// Created by erik.vonreis on 5/5/21.
//

#ifndef DAQD_TRUNK_SHMEM_IOMEM_H
#define DAQD_TRUNK_SHMEM_IOMEM_H

#include "drv/cdsHardware.h"

#define SHMEM_IOMEM_NAME "ipc"
#define SHMEM_IOMEM_SIZE_MB 32
#define SHMEM_IOMEM_SIZE ( SHMEM_IOMEM_SIZE_MB * 1024 * 1024 )
#define SHMEM_IOMEM_STRUCT IO_MEM_DATA

#endif // DAQD_TRUNK_SHMEM_IOMEM_H
