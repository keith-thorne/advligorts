//
// Created by erik.vonreis on 4/27/21.
//

#ifndef DAQD_TRUNK_AWG_DATA_H
#define DAQD_TRUNK_AWG_DATA_H

#include "daq_core_defs.h"
#include "shmem_header.h"

// maximum supported model rate in samples per second.
#define MAX_MODEL_RATE_SPS (1024 * 1024)

#define PAGE_DATUM_TYPE float

// Maximum page size: number of PAGE_DATUM_TYPE.  Awg is streamed one page per epoch per slot.
// _max_page_size is 2^20 samples per second ( > 1 megasample) * sizeof(double) / 16 epochs per second
#define _MAX_PAGE (MAX_MODEL_RATE_SPS / DAQ_NUM_DATA_BLOCKS_PER_SECOND)

typedef struct AWG_DATA_PAGE
{
    uint32_t status;   // 0 when written to by awg, 0xbad when not used.
    uint32_t _reserved[3];  // to align on 16 byte boundary.
    PAGE_DATUM_TYPE buf[_MAX_PAGE];
} AWG_DATA_PAGE;

// number channels of excitation that can be run at once per model (DCU).
#define AWG_SLOTS (9)

// number of epochs buffered per slot.
// If we wanted to increase this number beyond the number of epochs per second,
// then FIND_PAGE_INDEX would have to be changed.
#define AWG_EPOCHS_PER_SLOT (16)

// total number of data pages in the awg shared memory.
#define AWG_PAGE_COUNT (AWG_SLOTS * AWG_EPOCHS_PER_SLOT)

/**
 * Primary structure for awg shared memory
 * Contains all the pages passed between awg and the models.
 */
typedef struct AWG_DATA
{
    SHMEM_HEADER _header;
    AWG_DATA_PAGE page[AWG_PAGE_COUNT];
} AWG_DATA;

#define AWG_DATA_ID (1)  // structure identification.  Should be unique to the structure.
#define AWG_DATA_VERSION (2)

#define AWG_DATA_INIT(AD_PTR) SHMEM_SET_VERSION(AD_PTR, AWG_DATA_ID, AWG_DATA_VERSION)

// convert a slot number and epoch count into
#define FIND_PAGE_INDEX(SLOT, EPOCH) ((SLOT*AWG_EPOCHS_PER_SLOT) + (EPOCH % AWG_EPOCHS_PER_SLOT))

#endif // DAQD_TRUNK_AWG_DATA_H
