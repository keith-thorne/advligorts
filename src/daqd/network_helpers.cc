//
// Created by jonathan.hanks on 4/14/21.
//

#include "network_helpers.hh"

#include <stdexcept>
#include <cstring>
#include <string>

#include <regex.h>

#include <arpa/inet.h>
#include <netdb.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <iostream>

namespace net_helper
{
    static regex_t ip_regex;
    // regex_t dec_num_regex;

    // clang-format off
    // clang format has problems with raw string literals
    static const char* ipregexp = R"(^([0-9]{1,3}\.){3}[0-9]{1,3}$)";
    // clang-format on

    class regexp_compiler
    {
    public:
        regexp_compiler( )
        {
            auto rc = regcomp( &ip_regex, ipregexp, REG_NOSUB | REG_EXTENDED );
            if ( rc != 0 )
            {
                throw std::runtime_error( "Unable to compile ip regexp" );
            }
        }
        ~regexp_compiler( )
        {
            regfree( &ip_regex );
        }
    };
    static regexp_compiler initial_compile;

    int
    ip_str( const char* str ) try
    {
        const char* end = std::strchr( str, ':' );
        if ( end == nullptr )
        {
            end = str + strlen( str );
        }
        std::string addr_string( str, end );
        auto        result = get_inet_addr( addr_string.c_str( ) );
        return ( result != INADDR_NONE ? static_cast< int >( result ) : -1 );
    }
    catch ( ... )
    {
        return -1;
    }

    int
    port_str( const char* str )
    {
        const char* nc = nullptr;
        if ( ( nc = std::strchr( str, ':' ) ) != nullptr )
        {
            return htons( atoi( nc + 1 ) );
        }
        return htons( atoi( str ) );
    }

    int
    get_inet_addr( const char* str )
    {
        // See if the string is IP address
        if ( !is_valid_ip_address( str ) )
        {
            int             error;
            struct hostent* hp;
            int             ret;
            struct hostent  hent;
            char            buf[ 2048 ];

            memset( (void*)&hent, 1, sizeof( hent ) );
            // Try to resolve name into IP address
            if ( gethostbyname_r( str, &hent, buf, 2048, &hp, &error ) )
            {
                return -1;
            }
            if ( hp == nullptr )
            {
                return -1;
            }
            (void)memcpy( &ret, *hent.h_addr_list, sizeof( ret ) );
            return ret;
        }
        else
        {
            return inet_addr( str );
        }
    }

    int
    is_valid_dec_number( const char* str )
    {
        // Somehow the regular expression stuff is broken on amd64 linux...
        char*    endptr;
        long int res = strtol( str, &endptr, 10 );
        return endptr == ( str + strlen( str ) );
        // return step (str, dec_num_expbuf);
    }

    int
    is_valid_ip_address( const char* str )
    {
        auto rc = regexec( &ip_regex, str, 0, NULL, 0 );
        return rc == 0;
    }
} // namespace net_helper