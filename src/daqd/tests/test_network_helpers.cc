//
// Created by jonathan.hanks on 4/14/21.
//

#include "network_helpers.hh"

#include <string>
#include <vector>

#include <arpa/inet.h>

#include "catch.hpp"

TEST_CASE( "Test ip_str" )
{
    struct Test_case
    {
        std::string input;
        int         expected_result;
    };
    std::vector< Test_case > test_cases{
        { "127.0.0.1", static_cast< int >( htonl( 0x7f000001 ) ) },
        { "127.1.0.1", static_cast< int >( htonl( 0x7f010001 ) ) },
        { "127.0.0.1:9", static_cast< int >( htonl( 0x7f000001 ) ) },
        { "127.1.0.1:42", static_cast< int >( htonl( 0x7f010001 ) ) },
        { "not a hostname here", -1 },
        { "not a hostname here:8000", -1 },
    };

    for ( const auto& test_case : test_cases )
    {
        REQUIRE( net_helper::ip_str( test_case.input.c_str( ) ) ==
                 test_case.expected_result );
    }
}

TEST_CASE( "Test port_str" )
{
    struct Test_case
    {
        std::string input;
        int         expected_result;
    };
    std::vector< Test_case > test_cases{
        { "127.0.0.1:2000", htons( 2000 ) },
        { "127.1.0.1:1000", htons( 1000 ) },
        { "127.0.0.1:9", htons( 9 ) },
        { "127.1.0.1:42", htons( 42 ) },
        { "not a hostname here:8000", htons( 8000 ) },
        { "not a hostname here", 0 },
    };

    for ( const auto& test_case : test_cases )
    {
        REQUIRE( net_helper::port_str( test_case.input.c_str( ) ) ==
                 test_case.expected_result );
    }
}

TEST_CASE( "Test is_valid_dec_number" )
{
    struct Test_case
    {
        std::string input;
        int         expected_result;
    };
    std::vector< Test_case > test_cases{
        { "123", 1 },   { "123abc", 0 },
        { "0", 1 },     { "1", 1 },
        { "2", 1 },     { "3", 1 },
        { "4", 1 },     { "5", 1 },
        { "6", 1 },     { "7", 1 },
        { "8", 1 },     { "9", 1 },
        { "19923", 1 }, { "not a number", 0 },
    };

    for ( const auto& test_case : test_cases )
    {
        REQUIRE( net_helper::is_valid_dec_number( test_case.input.c_str( ) ) ==
                 test_case.expected_result );
    }
}

TEST_CASE( "Test is_valid_ip_address" )
{
    struct Test_case
    {
        std::string input;
        int         expected_result;
    };
    std::vector< Test_case > test_cases{
        { "127.0.0.1", 1 }, { "127.1.1.1", 1 }, { "abc.1.1.1", 0 },
        { "1.a.1.1", 0 },   { "1.1.b.1", 0 },   { "1.1.1.c", 0 },
        { "1.1", 0 },       { "1", 0 },         { "", 0 },
    };

    for ( const auto& test_case : test_cases )
    {
        REQUIRE( net_helper::is_valid_ip_address( test_case.input.c_str( ) ) ==
                 test_case.expected_result );
    }
}