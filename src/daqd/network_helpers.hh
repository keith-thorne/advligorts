//
// Created by jonathan.hanks on 4/14/21.
//

/*
 * This file is a collection of routines pulled out of daqd/net_writer that help
 * validate and parse network addresses or related items.  It was pulled out to
 * make these accessible to unit tests.
 */

#ifndef DAQD_TRUNK_NETWORK_HELPERS_HH
#define DAQD_TRUNK_NETWORK_HELPERS_HH

namespace net_helper
{
    /// Get IP address from the string in format `127.0.0.1:9090'
    /// Returns IP (value of inet_addr(3N)) on success, `-1' if failed.
    /// Fails if there is no IP address in the `str' or if `inet_addr()' failed.
    int ip_str( const char* str );

    /// Get port number from the string in the format `127.0.0.1:9090'
    /// returns the data in network byte order
    int port_str( const char* str );

    /// Match `str' against the regular expression to see if
    /// this is a valid IP address. If not try to resolv `str' into the IP
    /// address.
    int get_inet_addr( const char* str );

    int is_valid_dec_number( const char* str );

    /// See if the IP address is valid
    int is_valid_ip_address( const char* str );
} // namespace net_helper

#endif // DAQD_TRUNK_NETWORK_HELPERS_HH
