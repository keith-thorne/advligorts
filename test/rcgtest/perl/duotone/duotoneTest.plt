set term png
unset logscale; set logscale y
set title "Power Spectrum"
set xlabel 'Frequency (Hz)'
set ylabel 'Magnitude '
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100
set output "/tmp/rcgtest/images/duotone.png"
plot "/tmp/rcgtest/tmp/dtreference.data" using 1:2 smooth unique title "Reference", "/tmp/rcgtest/tmp/dt.data" using 1:2 smooth unique title "Test Data"
unset logscale
set title "Time Series"
set xlabel 'Cycle'
set ylabel 'Magnitude (ADC counts)'
set grid xtics ls 100
set grid ytics ls 100
set output "/tmp/rcgtest/images/duotoneNds.png"
plot "/opt/rtcds/rtscore/advligorts/rcgtest/perl/duotone/duotoneNdsRef.data" using 1:2 smooth unique title "Reference", "/tmp/rcgtest/tmp/duotoneNds.data" using 1:2 smooth unique title "Test Data"

