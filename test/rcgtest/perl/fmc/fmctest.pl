#!/usr/bin/perl -w -I /opt/cdscfg
##
##
use threads;
use threads::shared;
BEGIN {
	$epicsbase = $ENV{'EPICS_BASE'};
        $epicslib = $epicsbase . "/lib/linux-x86_64";
        $epicsperl = $epicsbase . "/lib/perl";
        print "EPICSBASE = $epicsbase \n";
        print "EPICSLIB = $epicslib \n";
        print "EPICSPERL = $epicsperl \n";
        push @INC,"/opt/cdscfg/tst/stddir.pl";
        push @INC,"$epicsperl";
        push @INC,"$epicslib";
	push @INC,"/opt/rtapps/debian10/perlmodules";
        push @INC,"/opt/rtcds/rtscore/advligorts/test/rcgtest/perl";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28/auto";
        push @INC,"/usr/lib/x86_64-linux-gnu/perl5/5.28";
        push @INC,"/opt/rtcds/rtscore/advligorts/src/perldaq";
}
#use stdenv;
use CaTools;
use Time::HiRes qw( usleep );
use POSIX qw(ceil floor);
use File::Path;

use strict;
(my $sec,my $min, my $hour, my $day, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);
$year += 1900;
my @abbr = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
my @strEpics = ("X2:FEC-112_RCG_VERSION","X2:FEC-112_MSG","X2:FEC-112_MSGDAQ","X2:FEC-112_RCG_VERSION");
my @strEpicsVals;
@strEpicsVals = caGet(@strEpics);

my $test_summary = "FAIL";
caPut("X2:IOP-SAM_TEST_STATUS","STARTING FMC TEST");
caPut("X2:IOP-SAM_TEST_STAT_12",2);
#print "\n\nSTARTING FMC TESTING ****************************************************************\n";
#print "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n\n";
print  "\n\nSTARTING FMC TESTING ************************************************************************\n";
print  "TIME: $hour:$min:$sec  $abbr[$mon]  $day $year \n";
print "RCG Version: $strEpicsVals[3]\n\n";


my @ctrlVals;
my @ctrlMons = ("X2:ATS-TIM16_T3_FMC_TST_CTRL_OUT");

my @filtMonVals;
my @filtMonChans = ("X2:ATS-TIM16_T3_FMC_TEST_MASK",
                    "X2:ATS-TIM16_T3_FMC_TEST_SWSTAT");
my @filtCtrlVals;
my @filtCtrlChans = ("X2:ATS-TIM16_T3_FMC_TEST_SWMASK",
                     "X2:ATS-TIM16_T3_FMC_TEST_SWREQ");

my @filtTestInChans = ("X2:ATS-TIM16_T3_FMC_TST_IN",
                	"X2:ATS-TIM16_T3_FMC_TST_CTRL_IN",
                	"X2:ATS-TIM16_T3_FMC_TST_MASK_IN");
my @filtTestInVals = (10.0,0,0);

my $isok;
my $status = 0;
my $ii;
my $err = 0;
my @testdata;
my @testswstat;

my @ctrlOutReq = (0x2000000,0x2000004,0x200000c,0x300000c,0x700000c,
		  0x700001c,0x70000dc,0x70003dc,0x7000fdc,0x7001fdc,
		  0x700dfdc,0x703dfdc,0x70fdfdc,0x73fdfdc,0x77fdfdc);
my @swstatReq = (0x0,0x400,0xc00,0x2c00,0x3c00,
		 0x3c01,0x3c03,0x3c07,0x3c0f,0x3c1f,
		 0x3c3f,0x3c7f,0x3cff,0x3dff,0x3fff);

# Need to clear out mask and req else alarm value with mask swstat readbacks
caPut("X2:ATS-TIM16_T3_FMC_TEST_SWMASK",0);
caPut("X2:ATS-TIM16_T3_FMC_TEST_SWREQ",0);

sleep(2);
# Initialize control settings
caPut(@filtTestInChans,@filtTestInVals);

# Set all of the filter switches to zero and check 
caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT OFF OFFSET OFF OUTPUT OFF LIMIT OFF HOLD OFF");
sleep(2);

@ctrlVals = caGet(@ctrlMons);
$testdata[0] = $ctrlVals[0];
@filtMonVals = caGet(@filtMonChans);
$testswstat[0] = $filtMonVals[1];
@filtCtrlVals = caGet(@filtCtrlChans);
my @compVals = (0,0,0.0,1.0,1.0);
$err = 0;
my $ctval = sprintf("0x%x",$ctrlVals[0]);
my $mask = sprintf("0x%x",$filtMonVals[0]);
my $swmask = sprintf("0x%x",$filtMonVals[1]);
my $swreq = sprintf("0x%x",$filtCtrlVals[0]);
my $swstat = sprintf("0x%x",$filtCtrlVals[1]);

print "Ctrl output = $ctval \n";
print "MASK = $mask SWMASK = $swmask SWREQ = $swreq SWSTAT = $swstat \n";


caPut("X2:IOP-SAM_TEST_STATUS","FMC TEST - REMOTE CONTROL");
# Check control out word when switches set from normal EPICS input
$err = 0;
sleep(2);

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET OFF OUTPUT OFF LIMIT OFF");
my $ctrlPf = "PASS";
my $swstatPf = "PASS";
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[1] = $ctrlVals[0];
$testswstat[1] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT OFF LIMIT OFF");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[2] = $ctrlVals[0];
$testswstat[2] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT OFF LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[3] = $ctrlVals[0];
$testswstat[3] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 OFF FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[4] = $ctrlVals[0];
$testswstat[4] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 OFF FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[5] = $ctrlVals[0];
$testswstat[5] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 OFF FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[6] = $ctrlVals[0];
$testswstat[6] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 OFF FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[7] = $ctrlVals[0];
$testswstat[7] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 ON FM5 OFF FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[8] = $ctrlVals[0];
$testswstat[8] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 OFF FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[9] = $ctrlVals[0];
$testswstat[9] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 OFF FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[10] = $ctrlVals[0];
$testswstat[10] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 OFF FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[11] = $ctrlVals[0];
$testswstat[11] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 OFF FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[12] = $ctrlVals[0];
$testswstat[12] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 ON FM10 OFF INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[13] = $ctrlVals[0];
$testswstat[13] = $filtMonVals[1];

caSwitch("X2:ATS-TIM16_T3_FMC_TEST","FM1 ON FM2 ON FM3 ON FM4 ON FM5 ON FM6 ON FM7 ON FM8 ON FM9 ON FM10 ON INPUT ON OFFSET ON OUTPUT ON LIMIT ON");
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[14] = $ctrlVals[0];
$testswstat[14] = $filtMonVals[1];

#Test results
for($ii=0;$ii<15;$ii++)
{
	if($testdata[$ii] != $ctrlOutReq[$ii]) {
		print "$testdata[$ii] $ctrlOutReq[$ii] val error \n";
		$ctrlPf = "FAIL";
		$status = -1;
	}
	if(($testswstat[$ii] & 0x7fff) != $swstatReq[$ii]) {
		print "$testswstat[$ii] $swstatReq[$ii] val error \n";
		$swstatPf = "FAIL";
		$status = -1;
	}
}

# Start Checking Local Controls **************************************************************************
caPut("X2:IOP-SAM_TEST_STATUS","FMC TEST - LOCAL CONTROL");
#initialize control input settings.
my @swSet = (0x1,0x3,0x7,0xf,0x1f,0x3f,0x7f,0xff,0x1ff,0x3ff,0x7ff,0xfff,0x1fff);
my $localctrlPf = "PASS";
my $localswstatPf = "PASS";

#Force turn off thru mask
@filtTestInVals = (10.0,0x0,0x3ff);
caPut(@filtTestInChans,@filtTestInVals);
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[15] = $ctrlVals[0];
$testswstat[15] = $filtMonVals[1];

#Switch on filters, starting with 1
my $jj = 16;
for($ii=0;$ii<10;$ii++)
{
	@filtTestInVals = (10.0,$swSet[$ii],0x3ff);
	caPut(@filtTestInChans,@filtTestInVals);
	sleep(2);
	@ctrlVals = caGet(@ctrlMons);
	@filtMonVals = caGet(@filtMonChans);
	$testdata[$jj] = $ctrlVals[0];
	$testswstat[$jj] = $filtMonVals[1];
	$jj ++;
}

$jj = 4;
#Test results
for($ii=15;$ii<25;$ii++)
{
	if($testdata[$ii] != $ctrlOutReq[$jj]) {
		print "$testdata[$ii] $ctrlOutReq[$jj] val error \n";
		$localctrlPf = "FAIL";
		$status = -1;
	}
	if(($testswstat[$ii] & 0x7fff) != $swstatReq[$jj]) {
		print "$testswstat[$ii] $swstatReq[$jj] val error \n";
		$localswstatPf = "FAIL";
		$status = -1;
	}
	$jj ++;
}

#Turn off mask and filters should stay ON
@filtTestInVals = (10.0,0,0x0);
caPut(@filtTestInChans,@filtTestInVals);
sleep(2);
@ctrlVals = caGet(@ctrlMons);
@filtMonVals = caGet(@filtMonChans);
$testdata[25] = $ctrlVals[0];
$testswstat[25] = $filtMonVals[1];
$ii = 25;
$jj = 14;
my $localremotePf = "PASS";
	if($testdata[$ii] != $ctrlOutReq[$jj]) {
		print "$testdata[$ii] $ctrlOutReq[$jj] val error \n";
		$localremotePf = "FAIL";
		$status = -1;
	}
	if(($testswstat[$ii] & 0x7fff) != $swstatReq[$jj]) {
		print "$testswstat[$ii] $swstatReq[$jj] val error \n";
		$localremotePf = "FAIL";
		$status = -1;
	}

if($status == 0) {
    $test_summary =  "PASS";
} else {
    $test_summary =  "FAIL";
}


# **********************************************************************************************************
# Print results ************************************************
open(OUTM,">/tmp/rcgtest/data/fmcTestData.dox") || die "cannot open test data file for writing";

print OUTM <<END;
/*!     \\page fmcControlTest Filter Module w/Control Test Data
*       \\verbatim
*************************************************************************************************

Filter Module w/Control (FMC) CODE TEST REPORT 

TIME: $hour:$min:$sec  $abbr[$mon]  $day $year

RCG version number: $strEpicsVals[3]

*************************************************************************************************

PURPOSE:
	Test local/remote switching of FMC part.

TEST OVERVIEW:
	Tests local/remote control settings of FMC by:
		1) Using standard FMC EPICS channels (remote control) to set/switch FMC parameters.
		2) Using embedded EPICS channels (local control) to set/switch FMC parameters via
		   the FMC control and mask inputs..

TEST REQUIREMENTS:
        This test was designed for use with the LLO DAQ Test Stand. It requires that the x1ats computer
	is running with the x2atstim16 model loaded.

TEST PROCEDURE:
        This test is automatically run by executing the fmctest.pl script, which is located in the
        /opt/rtcds/userapps/trunk/cds/test/scripts/filters directory. Using both remote and local 
	control, this script sets and reads back all of the FMC filter switches to verify proper
	operation.


TEST RESULTS:

-------------------------------------------------------------------------------------------------
      TEST                      				PASS/FAIL
-------------------------------------------------------------------------------------------------

	- Control switches set REMOTE (EPICS)			
		- Control output Values				$ctrlPf	
		- SWSTAT Values					    $swstatPf	

	- Control switches set LOCAL (RT Code)			
		- Control output Values				$localctrlPf	
		- SWSTAT Values					    $localswstatPf	

	- Switch from LOCAL to REMOTE Control	$localremotePf 

*************************************************************************************************
TEST SUMMARY:  $test_summary

        \\endverbatim
*\/


END
close OUTM;
print "TEST COMPLETE ***********************************************************************************\n";
system("cat /tmp/rcgtest/data/fmcTestData.dox");
if($status == 0) { 
    caPut("X2:IOP-SAM_TEST_STATUS","FMC TEST SUCCESS");
    caPut("X2:IOP-SAM_TEST_STAT_12",1);
}
if($status == -1) {
    caPut("X2:IOP-SAM_TEST_STAT_12",0);
    caPut("X2:IOP-SAM_TEST_STATUS","FMC TEST FAILED");
}
sleep(5);
caPut("X2:IOP-SAM_TEST_STATUS","TEST SYSTEM - IDLE");
exit($status);
