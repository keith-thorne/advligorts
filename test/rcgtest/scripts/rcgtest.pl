#!/usr/bin/perl -w 
##
##

use Switch;

my $test2run = $ARGV[0];

$rcgdir = $ENV{'RCG_DIR'};
$rcgdir .="/test/rcgtest";

$rcgtestdir = "/tmp/rcgtest";
$rcgtestdatadir = $rcgtestdir . "/data";
$rcgtestimagesdir = $rcgtestdir . "/images";
$rcgtesttmpdir = $rcgtestdir . "/tmp";
$rcgtestdocdir = $rcgtestdir . "/doc";
$rcgtestlatexdir = $rcgtestdocdir . "/latex";

#Create test data directories if they do not exist
unless( -e $rcgtestdir or mkdir $rcgtestdir) {
    die "unable to create test results directories\n";
}
unless( -e $rcgtestdatadir or mkdir $rcgtestdatadir) {
    die "unable to create test data directories\n";
}
unless( -e $rcgtestimagesdir or mkdir $rcgtestimagesdir) {
    die "unable to create test images directories\n";
}
unless( -e $rcgtesttmpdir or mkdir $rcgtesttmpdir) {
    die "unable to create test tmp directories\n";
}
unless( -e $rcgtestdocdir or mkdir $rcgtestdocdir) {
    die "unable to create test doc directories\n";
}
unless( -e $rcgtestlatexdir or mkdir $rcgtestlatexdir) {
    die "unable to create test doc directories\n";
}

#Determine which test to run
switch($test2run)
{
    case "daqfilter" {
        print "Starting daqfilter test\n";
        system("$rcgdir/perl/daqfilters/daqFiltTest.pl $rcgtestdir &");
    }
    case "decfilter" {
        print "Starting decfilter test\n";
        system("$rcgdir/perl/decfilters/decFiltTest.pl $rcgtestdir &");
    }
    case "duotone" {
        print "Starting duotone test\n";
        system("$rcgdir/perl/duotone/duotoneTest.pl $rcgtestdir &");
    }
    case "matrix" {
        print "Starting matrix test\n";
        system("$rcgdir/perl/matrix.pl $rcgtestdir &");
    }
    case "simparts" {
        print "Starting Simlink parts test\n";
        system("$rcgdir/perl/simPartsTest.pl $rcgtestdir &");
    }
    case "osc" {
        print "Starting oscillator part test\n";
        system("$rcgdir/perl/osc/oscTest.pl $rcgtestdir &");
    }
    case "fmc" {
        print "Starting filter module w/control test\n";
        system("$rcgdir/perl/fmc/fmctest.pl &");
    }
    case "fmc2" {
        print "Starting filter module w/control 2 test\n";
        system("$rcgdir/perl/fmc2/fmc2test.pl &");
    }
    case "sfm" {
        print "Starting standard IIR filter test\n";
        system("$rcgdir/perl/sfm/sfmTest.pl $rcgtestdir &");
    }
    case "inputfilter" {
        print "Starting input filter test\n";
        system("$rcgdir/perl/inputfilter/inputfilterTest.pl $rcgtestdir &");
    }
    case "ioptest" {
        print "Starting IOP test\n";
        system("$rcgdir/perl/rcgcore/ioptest.pl $rcgtestdir &");
    }
    case "dac" {
        print "Starting DAC test\n";
        system("$rcgdir/python/dactest/dactest.py &");
    }
    case "epics_in_ctl" {
        print "Starting EPICS INPUT W/Control test\n";
        system("$rcgdir/python/epicsparts/epicsInCtrl.py &");
    }
    case "exctp" {
        print "Starting EXCTP test\n";
        system("$rcgdir/python/exctp/extpTest.py &");
    }
    case "dackilltimed" {
        print "Starting DACKILL TIMED test\n";
        system("$rcgdir/python/dackill/dackilltimed.py &");
    }
    case "rampmuxmatrix" {
        print "Starting RampMuxMatrix test\n";
        system("$rcgdir/python/rampmuxmatrix/rampmuxmatrix.py &");
    }
    case "phase" {
        print "Starting Phase part test\n";
        system("$rcgdir/python/phase/phase.py &");
    }
    case "binaryIO" {
        print "Starting binary IO test\n";
        system("$rcgdir/python/binaryIO/biotest.py &");
    }
    case "testIO" {
        print "Starting IOP test\n";
        system("$rcgdir/perl/rcgcore/ioptest.pl $rcgtestdir ");
        sleep(30);
        print "Starting binary IO test\n";
        system("$rcgdir/python/binaryIO/biotest.py ");
        print "Starting DAC test\n";
        system("$rcgdir/python/dactest/dactest.py ");
        print "Starting duotone test\n";
        system("$rcgdir/perl/duotone/duotoneTest.pl $rcgtestdir ");
    }
    case "matrixtest" {
        print "Starting matrix test\n";
        system("$rcgdir/perl/matrix.pl $rcgtestdir ");
        print "Starting RampMuxMatrix test\n";
        system("$rcgdir/python/rampmuxmatrix/rampmuxmatrix.py ");
    }
    case "testfilters" {
        print "Starting decfilter test\n";
        system("$rcgdir/perl/decfilters/decFiltTest.pl $rcgtestdir ");
        sleep(5);
        print "Starting daqfilter test\n";
        system("$rcgdir/perl/daqfilters/daqFiltTest.pl $rcgtestdir ");
        sleep(5);
        print "Starting filter module w/control 2 test\n";
        system("$rcgdir/perl/fmc2/fmc2test.pl ");
        sleep(5);
        print "Starting filter module w/control test\n";
        system("$rcgdir/perl/fmc/fmctest.pl");
        sleep(5);
        print "Starting input filter test\n";
        system("$rcgdir/perl/inputfilter/inputfilterTest.pl $rcgtestdir ");
        sleep(5);
        print "Starting standard IIR filter test\n";
        system("$rcgdir/perl/sfm/sfmTest.pl $rcgtestdir ");
    }
    case "miscparts" {
        print "Starting DACKILL TIMED test\n";
        system("$rcgdir/python/dackill/dackilltimed.py ");
        print "Starting oscillator part test\n";
        system("$rcgdir/perl/osc/oscTest.pl $rcgtestdir ");
        print "Starting Phase part test\n";
        system("$rcgdir/python/phase/phase.py ");
        print "Starting EXCTP test\n";
        system("$rcgdir/python/exctp/extpTest.py ");
        print "Starting EPICS INPUT W/Control test\n";
        system("$rcgdir/python/epicsparts/epicsInCtrl.py ");
        print "Starting Simlink parts test\n";
        system("$rcgdir/perl/simPartsTest.pl $rcgtestdir ");
    }
    else {
        print "Unknown test\n";
    }
}

