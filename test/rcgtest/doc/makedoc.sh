#!/bin/bash

doxygen /opt/rtcds/rtscore/advligorts/doc/doxygen.cfg
cd /tmp/rcgtest/doc/latex
make pdf
cp /tmp/rcgtest/doc/latex/refman.pdf /opt/rtcds/rtscore/advligorts/test/rcgtest/doc/rcgtest.pdf
