set term png
set xlabel 'PHASE input (Degrees)'
set ylabel 'Magnitude '
set style line 100 lt 1 lc rgb "gray" lw 2
set style line 101 lt 1 lc rgb "gray" lw 1
set grid xtics ls 100
set grid ytics ls 100

set output "/tmp/rcgtest/images/phase.png"
plot "/tmp/rcgtest/tmp/phaseI.data" using 1:2 smooth unique title "PHASE_SIN Data", "/tmp/rcgtest/tmp/phaseQ.data" using 1:2 smooth unique title "PHASE_COS Data"
set output "/tmp/rcgtest/images/phaseOut.png"
plot "/tmp/rcgtest/tmp/phaseIout.data" using 1:2 smooth unique title "PHASE Out1 Data", "/tmp/rcgtest/tmp/phaseQout.data" using 1:2 smooth unique title "PHASE Out2 Data"
